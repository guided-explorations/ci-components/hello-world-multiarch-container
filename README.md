# Hello World Multiarch Container

Demonstrates a component that has a multiarch container associated with it. In .gitlab-ci.yml job peg_container_ver_in_template, demonstrates the best practice of pinning the same container and CI Component version together.

## If This Helps You, Please Star The Original Source Project
One click can help us keep providing and improving this component. If you find this information helpful, please click the star on this components original source here: [Project Details](https://gitlab.com/guided-explorations/ci-components/hello-world-container)

## Usage

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword and the container pegging tag so that you always get the CI code and container code
that have been designed, tested and released together.

There **might** be times to mismatch the CI Component and Container Versions - but it should be rare.

```yaml
include:
  - component: gitlab.com/components/hello-world-multiarch-container/hello-world-multiarch-container@<VERSION>
    inputs:
      HLWMC_CONTAINER_TAG: <VERSION>

#These are test lines to force underlying architecture to show that both work. 
#Your container pulls will automatically resolve both arches without forcing specific runner arches.
test-amd64-container:
  image: 
    name: registry.gitlab.com/guided-explorations/hello-world-multiarch-container:latest
    entrypoint: [""]
  tags:
    - saas-linux-small-amd64
  script:
    - |
      source /hello.sh

test-arm64-container:
  image: 
    name: registry.gitlab.com/guided-explorations/hello-world-multiarch-container:latest
    entrypoint: [""]
  tags:
    - saas-linux-medium-arm64
  stage: .post
  script:
    - |
      source /hello.sh

```

where `<VERSION>` is the tag of the version that you are adopting.

### Inputs and Configuration

| Input | Default value |    Type     | Description |
| ----- | ------------- | ----------- | ----------- |
| `stage` | test | CI Component Input     | The CI Stage to run the component in. |
| `HLWMC_CONTAINER_TAG` | `ALWAYS_SET_THIS_INPUT_TO_YOUR_COMPONENT_VERSION` | CI Component Input     | Pegs container for a stable dependency, but allows override. You must set this as an input. |
| `HLWMC_CONTAINER_TAG` | `Input: HLWC_CONTAINER_TAG` | CI Variable     | Passes through a variable in order to allow more flexible control for multiple calls in pipelines. |

### Debug Tracing

Set either CI_COMPONENT_TRACE or GitLab's global trace (CI_DEBUG_TRACE) to 'true' to get detailed debugging. More info: https://docs.gitlab.com/ee/ci/variables/#enable-debug-logging"

### Validation Test

If you simply include the component, the job log for a job called 'hello-world-container' should contain the logged text 'Hello CI Catalog World'

### Working Example Code Using This Component

- [Hello World Multiarch Container Working Example Code](https://gitlab.com/guided-explorations/ci-components/working-code-examples/hello-world-multiarch-container-component-test/)

### Related Component Projects

- [Hello World](https://gitlab.com/guided-explorations/ci-components/hello-world) - where to start if your component does not have a container.
- [Hello World Container](https://gitlab.com/guided-explorations/ci-components/hello-world-container) - where to start if your component has a single arch container.
- [Hello World Multiarch Container](https://gitlab.com/guided-explorations/ci-components/hello-world-multiarch-container) - where to start if your component needs to support multiarch containers. It was just that little more complex enough that I felt it should be distinct from the single arch container example.

## Using This Example as a Template for New Components

[How to cleanup commit history when using this as a scaffold for a new component](https://gitlab.com/guided-explorations/ci-components#using-an-existing-component-as-a-template-for-a-new-one)

## Component Building Builders Guide

[DarwinJS Component Builder Guide](https://gitlab.com/explore/catalog/guided-explorations/ci-components/gitlab-profile)

## Attributions and Credits

<a href="https://www.flaticon.com/free-icons/hello-world-container" title="hello world icons">Hello world icons created by IconBaandar - Flaticon</a>